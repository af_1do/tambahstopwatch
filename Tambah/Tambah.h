//
//  Tambah.h
//  Tambah
//
//  Created by Wito Chandra on 8/15/13.
//  Copyright (c) 2013 Wito Chandra. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tambah : NSObject

@property (nonatomic) NSString* value;
@property (nonatomic, strong) NSDate* timestamp;

@end
