//
//  ViewController.m
//  Tambah
//
//  Created by Wito Chandra on 8/15/13.
//  Copyright (c) 2013 Wito Chandra. All rights reserved.
//

#import "MainViewController.h"
#import "StorageViewController.h"
#import "Tambah.h"

@interface MainViewController ()

@property (nonatomic, strong) NSMutableArray* data;
@property (strong) NSDate* start;

@property double elapsed;
@property BOOL isRunning;

@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.data = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)eventStepperValueChanged:(id)sender {
    self.uiLabelCurrentValue.text = [NSString stringWithFormat:@"%.0f", self.uiStepper.value];
}

- (IBAction)eventButtonStartPressed:(id)sender {
    if(!self.isRunning) {
        [self startCounting];
    } else {
        [self stopCounting];
    }
}

- (IBAction)eventButtonResetPressed:(id)sender {
    self.uiLabelCurrentValue.text = @"0:00:00.000";
    self.elapsed = 0;
    self.start = [NSDate date];
}

- (IBAction)eventButtonSavePressed:(id)sender {
    Tambah* tambah = [[Tambah alloc] init];
    tambah.value = self.uiLabelCurrentValue.text;
    tambah.timestamp = [NSDate date];
    [self.data addObject:tambah];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"SEGUE_STORAGE"]) {
        ((StorageViewController*) segue.destinationViewController).data = self.data;
    }
}

-(void)startCounting {
    self.start = [NSDate dateWithTimeIntervalSinceNow:-self.elapsed];
    [self performSelectorInBackground:@selector(timerTask) withObject:nil];
    [self.uiButtonStart setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)stopCounting {
    self.isRunning = FALSE;
    [self.uiButtonStart setTitle:@"Start" forState:UIControlStateNormal];
}

-(void)timerTask {
    self.isRunning = TRUE;
    while(self.isRunning) {
        self.elapsed = [[NSDate date] timeIntervalSinceDate:self.start];
        int hour = self.elapsed / 3600;
        int min = (((int) self.elapsed % 3600) / 60);
        int sec = ((int) self.elapsed % 60);
        int mil = ((int)(self.elapsed * 1000) % 1000);
        [self performSelectorOnMainThread:@selector(setLabel:) withObject:[NSString stringWithFormat:@"%i:%02i:%02i.%03i", hour, min, sec, mil] waitUntilDone:TRUE];
        [NSThread sleepForTimeInterval:0.001];
    }
}

-(void)setLabel:(NSString*)label{
    self.uiLabelCurrentValue.text = label;
}
@end
