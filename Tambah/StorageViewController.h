//
//  StorageViewController.h
//  Tambah
//
//  Created by Wito Chandra on 8/15/13.
//  Copyright (c) 2013 Wito Chandra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StorageViewController : UITableViewController

@property (nonatomic, weak) NSMutableArray* data;

@end
