//
//  ViewController.h
//  Tambah
//
//  Created by Wito Chandra on 8/15/13.
//  Copyright (c) 2013 Wito Chandra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *uiLabelCurrentValue;
@property (weak, nonatomic) IBOutlet UIStepper *uiStepper;
@property (weak, nonatomic) IBOutlet UIButton *uiButtonStart;

- (IBAction)eventStepperValueChanged:(id)sender;
- (IBAction)eventButtonStartPressed:(id)sender;
- (IBAction)eventButtonResetPressed:(id)sender;
- (IBAction)eventButtonSavePressed:(id)sender;

@end
